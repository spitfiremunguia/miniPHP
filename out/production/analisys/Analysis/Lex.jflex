package Analysis;
import java.util.ArrayList;


%%
%{
	class Yytoken{
		public Yytoken(){

		}
	}
	List Code=new ArrayList();
	List ErrorCode=new ArrayList();
	String currentCode="";
%}


%public
%class Analyzer
%ignorecase
%char
%column
%line
%unicode


GLOBALS=((\$)(GLOBALS|_SERVER|_GET|_POST|_FILES|_COOKIE|_SESSION|_REQUEST|_ENV|HTTP_RAW_POST_DATA)(\[)(\"|\')({ID})(\"|\')(\]))
HTTP=((\$)(php_errormsg|http_response_header|argc|argv))
ID =  [a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*
VAR=((\$)({ID}))
INTEGER=((\+|-)?(0|[1-9][0-9]*)|(0((x|X)[0-9a-fA-F]+)|[0-7]+|(b|B)(0|1)+))
FLOAT=(([0-9]*(\.)[0-9]+)|([0-9]+(\.)[0-9]))
EFLOAT=((\+|-)?(([0-9]+|([0-9]*(\.)[0-9]+)|([0-9]+(\.)[0-9]))(e|E)(\+|-)?[0-9]+))
SSTRING=('([^'\\]|\\.)*')|(\"([^\"\\]|\\.)*\")
LOGIC=(\!|\&\&|\||<|>|=)
ARITMETHIC=((\+|-|\*|\/|\%|\*\*))
BITWISE=((\~|\||\^|<<|>>|\&))
COMPARISON=(==|===|\!=|<>|\!==|<=|>=|<=>)
INCREMENT=(\+\+|--)
COMENTARY=((\/\/(.*)))
MCOMENTARY="\/\*"~"\*\/"//esta es para multilinea
HCOMENTARY=((\#(.*)))
ORACLE=((\$)({ID})(\[)(\'|\")({ID})(\'|\")(\]))
META=(\(|\)|\{|\}|\[|\]|\;|\.|\,|\?|\:|\@|\_|\\)
NUMERICERR=({INTEGER})({ID})
HTMLTAGS=9pt
MULTERR="\/\*"~\n


%%

<YYINITIAL> {
//Spaces
//[\r\t\f/\s+/g]		{Code.add(yytext());currentCode+=yytext();}
[\n]        	{Code.add(yytext()+"\tLINESEPARATOR");currentCode+=yytext();}
[\r]			{Code.add(yytext()+"\tROW");currentCode+=yytext();}
[\t]			{Code.add(yytext()+"\tAB");currentCode+=yytext();}
[\f]			{Code.add(yytext()+"\tFTAB");currentCode+=yytext();}
[\s]			{Code.add(yytext()+"\tASPACE");currentCode+=yytext();}
[\g]			{Code.add(yytext()+"\tGSPACE");currentCode+=yytext();}
//[/\s+/g]		{Code.add(yytext());currentCode+=yytext();}
//main
"clone"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"die"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"empty"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"endswitch"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"final"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"goto"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"instanceof"	{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"namespace"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"protected"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"static"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"unset"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"abstract"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"callable"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"const"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"enddeclare"	{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"instadeof"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"new"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"public"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"use"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"case"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"endfor"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"eval"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"implements"	{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"interface"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"require"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"throw"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"var"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"array"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"catch"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"declare"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"endforeach"	{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"exit"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"include"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"isset"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"print"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"require_once"	{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"trait"			{Code.add(yytext().toLowerCase()+"\tESERVED");currentCode+=yytext().toLowerCase();}
"as"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"class"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"default"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"endif"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"extends"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"global"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"include_once"	{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"list"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"private"		{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
"try"			{Code.add(yytext().toLowerCase()+"\tRESERVED");currentCode+=yytext().toLowerCase();}
//compile time constants
"__CLASS__"		{Code.add(yytext().toUpperCase()+"\tCOMPILECONTS");currentCode+=yytext().toUpperCase();}
"__DIR__"		{Code.add(yytext().toUpperCase()+"\tCOMPILECONTS");currentCode+=yytext().toUpperCase();}
"__FILE__"		{Code.add(yytext().toUpperCase()+"\tCOMPILECONTS");currentCode+=yytext().toUpperCase();}
"__FUNCTION__"	{Code.add(yytext().toUpperCase()+"\tCOMPILECONTS");currentCode+=yytext().toUpperCase();}
"__LINE__"		{Code.add(yytext().toUpperCase()+"\tCOMPILECONTS");currentCode+=yytext().toUpperCase();}
"__METHOD__"	{Code.add(yytext().toUpperCase()+"\tCOMPILECONTS");currentCode+=yytext().toUpperCase();}
"__NAMESPACE__"	{Code.add(yytext().toUpperCase()+"\tCOMPILECONTS");currentCode+=yytext().toUpperCase();}
"__TRAIT__"		{Code.add(yytext().toUpperCase()+"\tCOMPILECONTS");currentCode+=yytext().toUpperCase();}
//control structures
"echo"			{Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"if"            {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"else"          {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"for"           {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"foreach"       {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"elseif"        {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"<?php"         {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"<?"            {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"?>"			{Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"while"         {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"endwhile"      {Code.add(yytext().toLowerCase()+"\tESERVERD");currentCode+=yytext().toLowerCase();}
"do"            {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"break"         {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"continue"      {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"switch"        {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"return"        {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"include"       {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"function"      {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"null"          {Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"and"			{Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"or"			{Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"xor"			{Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
"define"		{Code.add(yytext().toLowerCase()+"\tRESERVERD");currentCode+=yytext().toLowerCase();}
//RE's
{ORACLE}		{
					int i=yytext().indexOf("[");
					if(i==-1|yytext().indexOf("]")==-1){
						
						{ErrorCode.add("ERROR AT LINE: "+(yyline+1)+":"+yytext());}
					}
					else{
						if(yytext().contains("\"")|yytext().contains("\'")){
							String param=yytext().substring(0,i).toLowerCase()+yytext().substring(i,yytext().length()).toUpperCase();
							Code.add(param+"\tORACLE");
							currentCode+=param;
							}
						 	else{
								{ErrorCode.add("ERROR AT LINE: "+(yyline+1)+":"+yytext());}
																			 }
						}
				}
{GLOBALS}		{Code.add(yytext().toUpperCase()+"\tGLOBALS");currentCode+=yytext().toUpperCase();}
{HTTP}			{Code.add(yytext().toLowerCase()+"\tHTTPGLOBALS");currentCode+=yytext().toLowerCase();}
{ID}            {Code.add(yytext()+"\tID");currentCode+=yytext();}
{VAR}           {Code.add(yytext()+"\tvar");currentCode+=yytext();}				
{INTEGER}       {Code.add(yytext()+"\tinteger");currentCode+=yytext();}
{FLOAT}         {Code.add(yytext()+"\tfloat");currentCode+=yytext();}
{EFLOAT}        {Code.add(yytext()+"\tEFLOAT");currentCode+=yytext();}
{SSTRING}       {Code.add(yytext()+"\tSSTRING");currentCode+=yytext();}
{LOGIC}         {Code.add(yytext()+"\tLOGIC");currentCode+=yytext();}
{ARITMETHIC}    {Code.add(yytext()+"\tARITMETHIC");currentCode+=yytext();} 
{COMPARISON}	{Code.add(yytext()+"\tCOMPARISON");currentCode+=yytext();}
{INCREMENT}		{Code.add(yytext()+"\tINCREMENT");currentCode+=yytext();}
{BITWISE}		{Code.add(yytext()+"\tBITWISE");currentCode+=yytext();}
{COMENTARY}     {Code.add(yytext()+"\tCOMENTARY");currentCode+=yytext();}
{HCOMENTARY}    {Code.add(yytext()+"\tCOMENTARY");currentCode+=yytext();}
{MCOMENTARY}    {Code.add(yytext()+"\tCOMENTARY");currentCode+=yytext();}
{META}			{Code.add(yytext()+"\tMETA");currentCode+=yytext();}
{HTMLTAGS}		{Code.add(yytext()+"\tHTMLCODE");currentCode+=yytext();}
{NUMERICERR}	{ErrorCode.add("ERROR AT LINE: "+(yyline+1)+":"+yytext());}

{MULTERR}		{ErrorCode.add("ERROR AT LINE: "+(yyline+1)+":"+yytext());}

//ERRORS
.                      {ErrorCode.add("ERROR AT LINE: "+(yyline+1)+":"+yytext());}
}


