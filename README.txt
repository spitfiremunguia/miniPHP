                                miniPHP

requisitos:
alguna versión de Jflex, disponible en:http://jflex.de/download.html
versión reciente de java, disponible en:https://java.com/en/download/
El projecto fue desarrollado en el IDE IntelliJ v1.8 2018, disponible en:
https://www.jetbrains.com/idea/

                    Como correr el programa:

En la carpeta raíz del projecto se encuentra un archivo llamado miniPHP.jar.
dar doble click.
Si al dar doble click no funciona, ejecutar los siguientes pasos.
1)ir a la consola de su equipo
2)escribir la linea de comando: java -jar <dirección completa del miniPHP.jar>
    Ojo:La dirección va sin los <>

Que pasara al ejecutar:
1)el programa le pedira una dirección de un archivo que contenga codigo php
2)si el archivo no esta correcto (no existe) se le va a notificar y tendra que
  intentarlo de nuevo.
3)Si el archivo es correcto el programa analizara el contenido y decidira si se trata de codigo php
    si el archivo no contiene errores, se creara un archivo con el mismo nombre que la entrada, pero con extension .out
    este archivo .out estara en la misma dirección que el archivo entrada.
    el archivo contendra el código corregido (si hay algo que va en minusculas tenia que ir en mayusculas y viceversa)
4)si el archivo .out se creo correctamente entonces se le mostrara una opción para poder imprimir en consola los tokens que fueron evaluados
    si ingresa y, entonces se mostraran, si ingresa cualquier otra cosa no se mostraran.
5)si el archivo de entrana no conteniaa codigo php, entonces no se creara el archivo y se mostrara en consola una lista de errores

                    Como crear la clase Analizer.java
Analizar.java es la clase que maneja los autimatas finitos deterministas con los que es posible en análisis léxico.
para esto es necesario lo siguiente:
1)archivo con extensión .jflex que contenga la lógica de las expresiones regulares (este archivo esta contenido en la carpeta src del proyecto)
2)Jflex, este se descarga de la pagina dada en la parte de requisitos
3)Si se tiene windows, un archivo .bat

El archivo .bat es necesarario para ejecutar el programa contenido en Jflex
como crearlo:
En un archivo nuevo ingresar lo siguiente:


SET JAVA_HOME = "C:\Program Files\Java\jdk1.8.0_144\bin"    //cambiar la dirección por la dirección del java jdk en donde este en su equipo
SET PATH = %JAVA_HOME% ; %PATH%                             //esto se queda como esta
SET CLASSPATH = %JAVA_HOME%;                                //esto tambien se queda como esta
cd = C:\Users\david\IdeaProjects\analisys\src\Analysis      //esta es la dirección donde se creara la clase que maneje el análisis léxico
java -jar C:\jflex-1.6.1\jflex-1.6.1\lib\jflex-1.6.1.jar Lex.jflex  //esta dirección es donde esta el archivo jflex-1.6.1 (o cualquier otra versión) luego agregar un espacio y luego ingresar el nombre del archivo .Jflex que contiene todas las expresiones regulaes (este tiene que estar en la misma carpeta donde se creara la clase Analisys
pause                                                       //Esto se queda como esta

Ojo: guardar el archivo que acaba de crear con extensión .bat

6)Cuando el .bat este creado y funcional, este tomara la el archivo con extensión .Jflex de su proyeto y creara una clase llamada Analisys que hara el análisis léxico 
    con las expresiones regulares definidas.
